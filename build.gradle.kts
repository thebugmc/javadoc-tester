plugins {
    id("com.gradle.plugin-publish") version "1.2.1"
}

group = "net.thebugmc.gradle"
version = "1.1.0"

java.toolchain.languageVersion = JavaLanguageVersion.of(21)

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter:5.9.2")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher:1.9.2")
}

gradlePlugin {
    website = "https://gitlab.com/thebugmc/javadoc-tester"
    vcsUrl = "https://gitlab.com/thebugmc/javadoc-tester"

    plugins {
        create("javadoc-tester") {
            id = "net.thebugmc.gradle.javadoc-tester"
            implementationClass = "net.thebugmc.gradle.javadoctester.Main"
            displayName = "javadoc-tester"
            description = "Gradle plugin for testing code snippets in JavaDocs"
            tags.set(listOf("java", "javadoc", "test"))
        }
    }
}

tasks {
    test {
        useJUnitPlatform()
        testLogging {
            events("passed", "skipped", "failed", "standardOut", "standardError")
        }
    }
}