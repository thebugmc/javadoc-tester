# javadoc-tester

[![Gradle Plugin Portal](https://img.shields.io/gradle-plugin-portal/v/net.thebugmc.gradle.javadoc-tester?logo=gradle&label=Gradle%20Plugin%20Portal)](https://plugins.gradle.org/plugin/net.thebugmc.gradle.javadoc-tester)
[![MIT License](https://img.shields.io/badge/license-MIT-blue)](https://gitlab.com/thebugmc/javadoc-tester/blob/master/LICENSE-MIT)

Gradle plugin for testing `<pre>{@code...}</pre>` blocks in JavaDocs.

> *New in 1.1.0:* Support for the [JEP 413 `@snippet` blocks](https://openjdk.org/jeps/413) and the
> [JEP 467 `///` Markdown JavaDocs](https://openjdk.org/jeps/467) with its code blocks.

## Setup

> JUnit Jupiter is the only currently supported framework.

To set up your project for javadoc-tester, first add JUnit to your project like this (versions
listed here may be out of date):

```kts
dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter:5.9.2")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher:1.9.2")
}
```

Upon addition of JUnit to your project you can apply the plugin with the following options
(`build.gradle.kts` example):

```kts
plugin {
    id("net.thebugmc.gradle.javadoc-tester") version "..."
}
```

> Copy the proper plugin setup with the newest version from the [Gradle Plugin Portal](https://plugins.gradle.org/plugin/net.thebugmc.gradle.javadoc-tester).

Upon reloading the gradle project in your IDE the `/build/generated/sources/javadoc-tester/test`
will be used as a test source set. Now if you run the `:build` task, the `:test` portion of it will
go over the tests pulled out from your JavaDocs.

## Usage

To use javadoc-tester in your code for a setup project, you need to write `<pre>{@code...}</pre>`
blocks in your methods' JavaDocs. Mind that you need to write all necessary `import (static) ...`s
in your tests - this makes sure that the users of your examples may follow them properly.

Here is an example of a JavaDoc that will be JUnit-tested by javadoc-tester:

```java
/**
 * This method is an example for javadoc-tester.
 * 
 * <p>
 * <b>Examples</b>
 * {@snippet :
 * import static somepackage.SomeClass.someMethod;
 * 
 * assertEquals("Hello world", someMethod());
 * }
 *
 * @return A string "Hello world"
 */
public static String someMethod() {
    return "Hello world";
}
```

Upon generation of the test, it will be placed it in the previously mentioned
`/build/generated/sources/javadoc-tester/test` directory. The name of each test class matches the
following pattern: `Test_<javadoc's class>_<line>_<column>_<some Java hashCode>`.

The code is placed into a runnable method, with all of your unit testing framework's metadata
attached, as well as its necessary imports and a `throws Throwable` clause.

> If you really need to skip a snippet, make it not match `<pre>{@code...}</pre>`, or set language
> to something other than `java`. For example, `<pre> {@code` will not change the formatting much,
> yet the code block will not be detected by javadoc-tester.

> The generated test's class name is only used for you to tell them apart. Otherwise, it is not
> guaranteed to remain the same, unless the file of which JavaDocs are tested is not changed.

> Imports may only be at the top of the test. Any other imports will not be detected, and will be
> placed in the test's body, which is invalid syntax.

> Preservation of the order of imports is not guaranteed. Try to not cause import collisions. Look
> in the generated files to see how the test generation behaves with the JavaDoc that you wrote.

> JUnit Jupiter has an `import static org.junit.jupiter.api.Assertions.*;` as a prelude. Make sure
> you do not collide with the imported items in your tests, or else you risk not being able to use
> it as easily, e.g. you will have to specify the full package path to `Assertions` class or its
> methods.
