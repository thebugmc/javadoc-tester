package net.thebugmc.gradle.javadoctester;

import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestTest {
    public static final Logger log = Logging.getLogger(TestTest.class);

    @Test
    public void detectAllPre() {
        var source =
            """
            <pre>{@code
            import static java.lang.System.out;
            \s
            out.println("Hello world");
            }</pre>
            \s
            <pre>{@code
            System.out.println("Hello world");
            }</pre>
            """.stripIndent();

        var tests = net.thebugmc.gradle.javadoctester.Test.detectAllPre(
            log,
            Path.of("TestTest/detectAllPre"),
            source
        );

        assertEquals(
            List.of(
                new net.thebugmc.gradle.javadoctester.Test(
                    Set.of(new Import("java.lang.System.out", true)),
                    "detectAllPre",
                    """
                    out.println("Hello world");
                    """.stripIndent().strip(),
                    2,
                    1
                ),
                new net.thebugmc.gradle.javadoctester.Test(
                    Set.of(),
                    "detectAllPre",
                    """
                    System.out.println("Hello world");
                    """.stripIndent().strip(),
                    8,
                    1
                )
            ),
            tests
        );
    }

    @Test
    public void detectAllSnippet() {
        var source =
            """
            {@snippet :
            import static java.lang.System.out;
            \s
            out.println("Hello world");
            }
            \s
            {@snippet :
            System.out.println("Hello world");
            }
            """.stripIndent();

        var tests = net.thebugmc.gradle.javadoctester.Test.detectAllSnippet(
            log,
            Path.of("TestTest/detectAllSnippet"),
            source
        );

        assertEquals(
            List.of(
                new net.thebugmc.gradle.javadoctester.Test(
                    Set.of(new Import("java.lang.System.out", true)),
                    "detectAllSnippet",
                    """
                    out.println("Hello world");
                    """.stripIndent().strip(),
                    2,
                    1
                ),
                new net.thebugmc.gradle.javadoctester.Test(
                    Set.of(),
                    "detectAllSnippet",
                    """
                    System.out.println("Hello world");
                    """.stripIndent().strip(),
                    8,
                    1
                )
            ),
            tests
        );
    }

    @Test
    public void detectAllMarkdown() {
        var source =
            """
            ```java
            import static java.lang.System.out;
            \s
            out.println("Hello world");
            ```
            \s
            ```java
            System.out.println("Hello world");
            ```
            """.stripIndent();

        var tests = net.thebugmc.gradle.javadoctester.Test.detectAllMarkdown(
            log,
            Path.of("TestTest/detectAllMarkdown"),
            source
        );

        assertEquals(
            List.of(
                new net.thebugmc.gradle.javadoctester.Test(
                    Set.of(new Import("java.lang.System.out", true)),
                    "detectAllMarkdown",
                    """
                    out.println("Hello world");
                    """.stripIndent().strip(),
                    2,
                    1
                ),
                new net.thebugmc.gradle.javadoctester.Test(
                    Set.of(),
                    "detectAllMarkdown",
                    """
                    System.out.println("Hello world");
                    """.stripIndent().strip(),
                    8,
                    1
                )
            ),
            tests
        );
    }
}
