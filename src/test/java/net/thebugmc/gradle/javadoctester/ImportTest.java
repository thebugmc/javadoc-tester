package net.thebugmc.gradle.javadoctester;

import net.thebugmc.gradle.javadoctester.Import.ParseException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ImportTest {
    @Test
    public void parse() throws ParseException {
        var imp1 = Import.parse("import java.lang.System");
        var imp2 = Import.parse("import static java.lang.System.out");
        assertEquals(new Import("java.lang.System", false), imp1);
        assertEquals(new Import("java.lang.System.out", true), imp2);
    }

    @Test
    public void parseFail() {
        assertThrows(ParseException.class, () -> Import.parse("import.lang.System"));
        assertThrows(ParseException.class, () -> Import.parse("impart java.lang.System"));
        assertThrows(ParseException.class, () -> Import.parse("import stating java.lang.System"));
    }
}
