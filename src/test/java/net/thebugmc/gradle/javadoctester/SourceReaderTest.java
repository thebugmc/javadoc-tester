package net.thebugmc.gradle.javadoctester;

import net.thebugmc.gradle.javadoctester.SourceReader.AwarePair;
import net.thebugmc.gradle.javadoctester.SourceReader.Group;
import net.thebugmc.gradle.javadoctester.SourceReader.ReadChar;
import net.thebugmc.gradle.javadoctester.SourceReader.ReadUntil;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SourceReaderTest {
    public static final Logger log = Logging.getLogger(SourceReaderTest.class);

    @Test
    public void readChar() {
        var str = "Te\nst";
        var reader = new SourceReader(str, Path.of("SourceReaderTest/readChar"));

        assertEquals(
            Optional.of(new ReadChar(str.charAt(0), 1, 1)),
            reader.read()
        );
        assertEquals(
            Optional.of(new ReadChar(str.charAt(1), 1, 2)),
            reader.read()
        );
        reader.read(); // str.charAt(2); let's just assume `\n`'s position is undefined
        assertEquals(
            Optional.of(new ReadChar(str.charAt(3), 2, 1)),
            reader.read()
        );
        assertEquals(
            Optional.of(new ReadChar(str.charAt(4), 2, 2)),
            reader.read()
        );
        assertEquals(
            Optional.empty(),
            reader.read()
        );
    }

    @Test
    public void readUntil() {
        var str = "start{abc{}def}}";

        var sourcePath = Path.of("SourceReaderTest/readUntil");
        var reader = new SourceReader(str, sourcePath);
        assertEquals(
            Optional.of(new ReadUntil("start{abc{", 1, 1)),
            reader.readUntil(log, "}")
        );

        reader = new SourceReader(str, sourcePath);
        assertEquals(
            Optional.of(new ReadUntil("start{abc{}def}", 1, 1)),
            reader.readUntil(log, "}", Optional.of(new AwarePair("{", "}")))
        );
    }

    @Test
    public void readGroup() {
        var str = "...start {:{:}:} :\n    def{}def}...";

        var reader = new SourceReader(str, Path.of("SourceReaderTest/readGroup"));

        assertEquals(
            Optional.of(new Group(
                new ReadUntil("def{}def", 2, 5),
                Optional.of(" {:{:}:} ")
            )),
            reader.readGroup(
                log,
                "start",
                Optional.of(":"),
                "}",
                Optional.of(new AwarePair("{", "}")),
                "group"
            )
        );
    }
}
