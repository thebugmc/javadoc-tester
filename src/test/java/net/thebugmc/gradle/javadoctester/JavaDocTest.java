package net.thebugmc.gradle.javadoctester;

import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JavaDocTest {
    public static final Logger log = Logging.getLogger(JavaDocTest.class);

    @Test
    public void detectAll() throws IOException {
        var reader = new BufferedReader(new StringReader(
            """
            /**
             * javadoc1
             */
            \s
            ///
            /// javadoc2
            /// javadoc3
            ///
            \s
            /**
             * javadoc4
             * javadoc5
             */
            \s
            ///
            /// javadoc6
            ///
            """.stripIndent()
        ));

        var javadocs = JavaDoc.detectAll(log, reader);

        assertEquals(
            List.of(
                new JavaDoc("javadoc1", false),
                new JavaDoc("javadoc2\njavadoc3", true),
                new JavaDoc("javadoc4\njavadoc5", false),
                new JavaDoc("javadoc6", true)
            ),
            javadocs
        );
    }
}
