package net.thebugmc.gradle.javadoctester;

import org.gradle.api.logging.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public record JavaDoc(String lines, boolean markdown) {
    public static final char JAVADOC_SLASH = '/';
    public static final String JAVADOC_LINE = "*";
    public static final String JAVADOC_START = JAVADOC_SLASH + JAVADOC_LINE.repeat(2);
    public static final String JAVADOC_END = JAVADOC_LINE + JAVADOC_SLASH;
    public static final String MARKDOWN_LINE = "///";

    public static List<JavaDoc> detectAll(
        Logger log,
        BufferedReader lines
    ) throws IOException {
        var result = new ArrayList<JavaDoc>();

        enum State {
            NONE,
            JAVADOC,
            MARKDOWN
        }

        var current = new ArrayList<String>();
        var state = State.NONE;

        var lineNum = 0;
        var line = lines.readLine();

        readLine:
        while (line != null) {
            line = line.strip();

            switch (state) {
                // if none, check if we can start the doc, and then
                case NONE -> {
                    var without = Optional.<String>empty();

                    if (line.startsWith(JAVADOC_START)) {
                        state = State.JAVADOC;
                        without = Optional.of(
                            line.substring(JAVADOC_START.length())
                        );
                    } else if (line.startsWith(MARKDOWN_LINE)) {
                        state = State.MARKDOWN;
                        without = Optional.of(
                            line.substring(MARKDOWN_LINE.length())
                        );
                    }

                    // add the line to current if it's not like /// /// /// empty first lines
                    var finalCurrent = current;
                    without
                        .filter(str -> !str.isBlank() || !finalCurrent.isEmpty())
                        .ifPresent(finalCurrent::add);
                }

                // if javadoc, ensure line contains no javadoc end, and starts with *
                case JAVADOC -> {
                    if (!line.startsWith(JAVADOC_LINE)) {
                        log.warn(
                            "Javadoc line [%s] does not start with `%s`"
                                .formatted(lineNum, JAVADOC_LINE)
                        );

                        state = State.NONE;
                        current = new ArrayList<>();

                        // read comment to the end
                        while ((line = lines.readLine()) != null)
                            if (line.contains(JAVADOC_END)) {
                                line = line.substring(0, line.indexOf(JAVADOC_END));
                                continue readLine;
                            }

                        // no lines left
                        log.warn("Javadoc ends with the file without `%s`".formatted(JAVADOC_END));
                        return result;
                    }

                    line = line.substring(JAVADOC_LINE.length());

                    if (line.startsWith(JAVADOC_SLASH + "")) {
                        state = State.NONE;

                        while (current.getLast().isBlank())
                            current.removeLast();

                        result.add(new JavaDoc(
                            String.join("\n", current)
                                .stripIndent()
                                .strip(),
                            false
                        ));
                        current = new ArrayList<>();
                        continue;
                    }

                    current.add(line);
                }

                case MARKDOWN -> {
                    if (!line.startsWith(MARKDOWN_LINE)) {
                        state = State.NONE;

                        while (current.getLast().isBlank())
                            current.removeLast();

                        result.add(new JavaDoc(
                            String.join("\n", current)
                                .stripIndent()
                                .strip(),
                            true
                        ));
                        current = new ArrayList<>();
                        continue;
                    }

                    line = line.substring(MARKDOWN_LINE.length());

                    current.add(line);
                }
            }

            line = lines.readLine();
            lineNum++;
        }

        switch (state) {
            case NONE -> {
            }
            case JAVADOC ->
                log.warn("Javadoc ends with the file without `%s`".formatted(JAVADOC_END));
            case MARKDOWN -> {
                while (current.getLast().isBlank())
                    current.removeLast();

                result.add(new JavaDoc(
                    String.join("\n", current)
                        .stripIndent()
                        .strip(),
                    true
                ));
            }
        }

        return result;
    }
}
