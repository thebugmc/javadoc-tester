package net.thebugmc.gradle.javadoctester;

import org.gradle.api.logging.Logger;

import java.nio.file.Path;
import java.util.Optional;

public final class SourceReader {
    private final String source;
    private final Path sourcePath;

    public SourceReader(String source, Path sourcePath) {
        this.source = source;
        this.sourcePath = sourcePath;
    }

    public Path sourcePath() {
        return sourcePath;
    }

    //
    // Position
    //

    private int pos = 0, line = 1, column = 1;

    public boolean eof() {
        return pos >= source.length();
    }

    public void skip(int length) {
        while (length-- > 0)
            if (read().isEmpty())
                return;
    }

    public boolean pointingAt(String s) {
        return source.startsWith(s, pos);
    }

    public boolean pointingAtRegex(String regex) {
        return source.substring(pos).matches("(?s)^" + regex + ".*");
    }

    private static void ensureCorrectLineColumn(int line, int column) {
        if (line < 1)
            throw new IllegalArgumentException("line must be >= 1");
        if (column < 1)
            throw new IllegalArgumentException("column must be >= 1");
    }

    //
    // readChar
    //

    public record ReadChar(char ch, int line, int column) {
        public ReadChar {
            ensureCorrectLineColumn(line, column);
        }
    }

    public Optional<ReadChar> read() {
        if (pos >= source.length())
            return Optional.empty();

        var ch = source.charAt(pos++);
        var res = new ReadChar(ch, line, column++);

        if (ch == Utils.LINE_SEPARATOR) {
            line++;
            column = 1;
        }

        return Optional.of(res);
    }

    //
    // readUntil
    //

    public record ReadUntil(String string, int line, int column) {
        public ReadUntil {
            ensureCorrectLineColumn(line, column);
        }
    }

    public record AwarePair(String start, String end) {
    }

    public Optional<ReadUntil> readUntil(Logger log, String end) {
        return readUntil(log, end, Optional.empty());
    }

    public Optional<ReadUntil> readUntil(Logger log, String end, Optional<AwarePair> awarePair) {
        var startPos = pos;
        var startLine = line;
        var startColumn = column;

        var insidePairs = 0;
        while (true) {
            if (awarePair.isPresent()) {
                var pair = awarePair.get();

                if (pointingAt(pair.start)) {
                    insidePairs++;
                    skip(pair.start.length());
                }

                if (insidePairs > 0) {
                    if (eof()) {
                        log.warn(
                            "File ended while reading an inner `%s`-`%s` pair in `%s`"
                                .formatted(pair.start, pair.end, sourcePath)
                        );
                        return Optional.empty();
                    }

                    if (pointingAt(pair.end)) {
                        insidePairs--;
                        skip(pair.end.length());
                    } else skip(1);

                    continue;
                }
            }

            if (pointingAt(end))
                return Optional.of(new ReadUntil(
                    source.substring(startPos, pos),
                    startLine,
                    startColumn
                ));

            if (read().isEmpty())
                return Optional.empty();
        }
    }

    //
    // readGroup
    //

    public record Group(ReadUntil content, Optional<String> preInnerStart) {
    }

    /**
     * Read the next group.
     *
     * @param start       The start of the group (e.g. <code>&#123;@snippet</code>, <code>&lt;pre&gt;&#123;@code</code>, etc)
     * @param innerStart  The inner start (e.g. {@code :} for {@code @snippet})
     * @param end         The end of the group
     * @param awarePair   The inner part to be aware of (you have to enter and leave it enough times before this method can process anything).
     * @param warnContext The context of the warning (e.g. {@code "JavaDoc @snippet's code"})
     * @return The group between start and end.
     */
    public Optional<Group> readGroup(
        Logger log,
        String start,
        Optional<String> innerStart,
        String end,
        Optional<AwarePair> awarePair,
        String warnContext
    ) {
        var preStartOpt = readUntil(log, start);
        if (preStartOpt.isEmpty())
            return Optional.empty();
        skip(start.length());

        var preInnerStart = Optional.<String>empty();
        if (innerStart.isPresent()) {
            var preInnerStartOpt = readUntil(log, innerStart.get(), awarePair);
            if (preInnerStartOpt.isEmpty()) {
                log.warn(
                    "Missing `%s` to start a %s in `%s`"
                        .formatted(innerStart.get(), warnContext, sourcePath)
                );
                return Optional.empty();
            }
            skip(innerStart.get().length());

            preInnerStart = Optional.of(preInnerStartOpt.get().string());
        }

        while (pointingAtRegex("[ \n\t]")) skip(1);

        var contentOpt = readUntil(log, end, awarePair);
        if (contentOpt.isEmpty()) {
            log.warn(
                "Missing `%s` to end a %s in `%s`"
                    .formatted(end, warnContext, sourcePath)
            );
            return Optional.empty();
        }
        skip(end.length());

        return Optional.of(new Group(
            contentOpt.get(),
            preInnerStart
        ));
    }
}
