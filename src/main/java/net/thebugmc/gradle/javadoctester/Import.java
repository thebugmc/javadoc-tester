package net.thebugmc.gradle.javadoctester;

public record Import(String path, boolean isStatic) {
    public static final class ParseException extends Exception {
        public ParseException(String message) {
            super(message);
        }
    }

    private static final String IMPORT_KEYWORD = "import";
    private static final String STATIC_KEYWORD = "static";
    private static final char IMPORT_WORD_SEPARATOR = ' ';

    public static Import parse(String string) throws ParseException {
        // 1. `import` until space
        var firstSpace = string.indexOf(IMPORT_WORD_SEPARATOR);
        if (firstSpace == -1)
            throw new ParseException("missing space in an import");

        var importKeyword = string.substring(0, firstSpace);
        if (!importKeyword.equals(IMPORT_KEYWORD))
            throw new ParseException("expected `import` keyword in an import");
        string = string.substring(firstSpace);

        // 2. skip spaces between `import` and `static` or `path.to.import`
        while (string.indexOf(IMPORT_WORD_SEPARATOR) == 0)
            string = string.substring(1);

        // 3. `static` or `path.to.import`
        var secondSpace = string.indexOf(IMPORT_WORD_SEPARATOR);
        if (secondSpace == -1)
            return new Import(string, false); // no `static`; `path.to.import` = `string`

        var staticKeyword = string.substring(0, secondSpace);
        if (!staticKeyword.equals(STATIC_KEYWORD))
            throw new ParseException("expected `static` keyword in an import");
        string = string.substring(secondSpace);

        // 4. spaces between `static` and `path.to.import`
        while (string.indexOf(IMPORT_WORD_SEPARATOR) == 0)
            string = string.substring(1);

        return new Import(string, true); // `static`; `path.to.import` = `string`
    }

    public String toString() {
        return "import " + (isStatic ? "static " : "") + path;
    }
}
