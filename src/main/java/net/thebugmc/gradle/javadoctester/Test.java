package net.thebugmc.gradle.javadoctester;

import net.thebugmc.gradle.javadoctester.SourceReader.AwarePair;
import net.thebugmc.gradle.javadoctester.SourceReader.ReadUntil;
import org.gradle.api.logging.Logger;

import java.nio.file.Path;
import java.util.*;

public record Test(Set<Import> imports, String filenameWithoutExtension, String source, int line, int column) {
    public static class ParseException extends Exception {
        public ParseException(String message) {
            super(message);
        }
    }

    public static final char IMPORT_SEPARATOR = ';';

    public static Test parse(
        Logger log,
        String filename,
        ReadUntil testContent
    ) throws ParseException {
        var string = testContent.string();

        var imports = new HashSet<Import>();
        while (string.strip().startsWith("import")) {
            var end = string.indexOf(IMPORT_SEPARATOR);
            if (end == -1)
                throw new ParseException("missing semicolon after an import");

            var impString = string.substring(0, end).strip();

            Import imp;
            try {
                imp = Import.parse(impString);
            } catch (Import.ParseException e) {
                log.warn("Failed to parse import `%s`: %s".formatted(
                    impString.replace("\n", " "),
                    e.getMessage()
                ));
                continue;
            }

            if (!imports.add(imp))
                throw new ParseException("duplicate import");

            string = string.substring(end + 1);
        }

        var code = (string + "")
            .stripIndent()
            .strip();

        return new Test(imports, filename, code + "", testContent.line(), testContent.column());
    }

    //
    // detectAllPre
    //

    public static final String PRE_START = "<pre>{@code";
    public static final String PRE_END = "}</pre>";

    public static List<Test> detectAllPre(Logger log, Path sourcePath, String source) {
        var tests = new ArrayList<Test>();

        var reader = new SourceReader(source, sourcePath);

        while (!reader.eof()) {
            var testContentOpt = reader.readGroup(
                log,
                PRE_START,
                Optional.empty(),
                PRE_END,
                Optional.of(new AwarePair(OPEN_BRACE + "", CLOSE_BRACE + "")),
                "JavaDoc test"
            );
            if (testContentOpt.isEmpty())
                continue;
            var testContent = testContentOpt.get();

            try {
                tests.add(parse(
                    log,
                    filenameWithoutExtension(sourcePath),
                    testContent.content()
                ));
            } catch (ParseException e) {
                logFailedToParse(log, sourcePath, testContent.content(), e);
            }
        }

        return tests;
    }

    //
    // detectAllSnippet
    //

    public static final String SNIPPET_START = "{@snippet";
    public static final char SNIPPET_CODE_START = ':';
    public static final char OPEN_BRACE = '{';
    public static final char CLOSE_BRACE = '}';

    public static List<Test> detectAllSnippet(Logger log, Path sourcePath, String source) {
        var tests = new ArrayList<Test>();

        var reader = new SourceReader(source, sourcePath);

        while (!reader.eof()) {
            var testContentOpt = reader.readGroup(
                log,
                SNIPPET_START,
                Optional.of(SNIPPET_CODE_START + ""),
                CLOSE_BRACE + "",
                Optional.of(new AwarePair(OPEN_BRACE + "", CLOSE_BRACE + "")),
                "JavaDoc @snippet's code"
            );
            if (testContentOpt.isEmpty())
                continue;
            var testContent = testContentOpt.get();

            // TODO add support for `{@snippet file="..."}` and `{@snippet class="..."}`
            // TODO ensure `lang=java` or none

            try {
                tests.add(parse(
                    log,
                    filenameWithoutExtension(sourcePath),
                    testContent.content()
                ));
            } catch (ParseException e) {
                logFailedToParse(log, sourcePath, testContent.content(), e);
            }
        }

        return tests;
    }

    //
    // detectAllMarkdown
    //

    public static final String MARKDOWN_START = "```";
    public static final String MARKDOWN_END = "```";

    public static List<Test> detectAllMarkdown(Logger log, Path sourcePath, String source) {
        var tests = new ArrayList<Test>();

        var reader = new SourceReader(source, sourcePath);

        while (!reader.eof()) {
            var testContentOpt = reader.readGroup(
                log,
                MARKDOWN_START,
                Optional.of("\n"),
                MARKDOWN_END,
                Optional.empty(),
                "JavaDoc Markdown test"
            );
            if (testContentOpt.isEmpty())
                continue;
            var testContent = testContentOpt.get();

            // TODO ensure ```` ```java ```` or none

            try {
                tests.add(parse(
                    log,
                    filenameWithoutExtension(sourcePath),
                    testContent.content()
                ));
            } catch (ParseException e) {
                logFailedToParse(log, sourcePath, testContent.content(), e);
            }
        }

        return tests;
    }

    //
    // Util
    //

    public static final char FILE_EXTENSION_SEPARATOR = '.';

    private static String filenameWithoutExtension(Path path) {
        var filename = path.getFileName() + "";
        var extPos = filename.lastIndexOf(FILE_EXTENSION_SEPARATOR);
        if (extPos != -1)
            return filename.substring(0, extPos);
        return filename;
    }

    private static void logFailedToParse(
        Logger log,
        Path sourcePath,
        ReadUntil testContent,
        ParseException e
    ) {
        log.warn(
            "Failed to parse test in `%s` at [%d:%d]: %s"
                .formatted(sourcePath, testContent.line(), testContent.column(), e.getMessage())
        );
    }
}
