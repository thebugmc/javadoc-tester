package net.thebugmc.gradle.javadoctester;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.logging.Logger;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.tasks.SourceSetContainer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static net.thebugmc.gradle.javadoctester.Utils.clearDir;
import static org.gradle.api.tasks.SourceSet.MAIN_SOURCE_SET_NAME;
import static org.gradle.api.tasks.SourceSet.TEST_SOURCE_SET_NAME;

public class Main implements Plugin<Project> {
    public void apply(Project target) {
        try {
            applyWithExceptions(target.getLogger(), target);
        } catch (IOException e) {
            //noinspection ProhibitedExceptionThrown
            throw new RuntimeException(e);
        }
    }

    public void applyWithExceptions(Logger log, Project target) throws IOException {
        var testDirPath = ensureTestDirExists(log, target);
        var sourceSets = sourceSets(log, target);

        log.info("Adding generated dir to `test` source sets");
        sourceSets
            .getByName(TEST_SOURCE_SET_NAME)
            .getJava()
            .srcDir(testDirPath);

        // It would be nice to make a `generateJavaDocTests` task instead of calling this here
        // directly, with attaching it as a dependency for the `:test` task, but apparently the
        // sources are read only on reload, not upon task completion. I do not know how to fix that,
        // or if a fix is needed/possible. Apparently this is run fine upon `:build`, with correct
        // detection of the newly generated sources.
        generateTestFiles(log, target);
    }

    public static Path ensureTestDirExists(Logger log, Project target) throws IOException {
        log.info("Ensuring a directory for the tests exists");
        var testDirPath = target
            .getLayout()
            .getBuildDirectory()
            .dir("generated/sources/javadoc-tester/test")
            .get()
            .getAsFile()
            .toPath();
        clearDir(testDirPath);
        Files.createDirectories(testDirPath);
        return testDirPath;
    }

    public static SourceSetContainer sourceSets(Logger log, Project target) {
        log.info("Detecting source sets");
        return target
            .getExtensions()
            .getByType(JavaPluginExtension.class)
            .getSourceSets();
    }

    public static List<Test> enumerateMainSourceSetAndDetectTests(
        Logger log,
        SourceSetContainer sourceSets
    ) throws IOException {
        log.info("Enumerating `main` source set and detecting its JavaDocs");
        var mainSourceSet = sourceSets.getByName(MAIN_SOURCE_SET_NAME);

        var files = new HashSet<Path>();
        var tests = new ArrayList<Test>();
        for (var dirFile : mainSourceSet.getAllSource().getSrcDirs()) {
            var dirPath = dirFile.toPath();
            if (Files.isDirectory(dirPath))
                try (var stream = Files.walk(dirPath)) {
                    for (var path : (Iterable<Path>) stream::iterator)
                        if (Files.isRegularFile(path))
                            files.add(path);
                }
        }

        for (var path : files)
            try (var reader = Files.newBufferedReader(path)) {
                for (var doc : JavaDoc.detectAll(log, reader))
                    if (doc.markdown())
                        tests.addAll(Test.detectAllMarkdown(log, path, doc.lines()));
                    else {
                        tests.addAll(Test.detectAllPre(log, path, doc.lines()));
                        tests.addAll(Test.detectAllSnippet(log, path, doc.lines()));
                    }
            }
        log.info("Detected %d JavaDoc tests in %d files".formatted(tests.size(), files.size()));

        return tests;
    }

    public static void generateTestFiles(
        Logger log,
        Path testDirPath,
        List<Test> tests
    ) throws IOException {
        log.info("Generating test files");
        for (var test : tests) {
            var result = new StringBuilder();

            // collect imports
            var imports = new HashSet<Import>();

            // add prelude // TODO accept any framework
            imports.add(new Import("org.junit.jupiter.api.Assertions.*", true));

            imports.addAll(test.imports());
            for (var imp : imports)
                result.append("%s;\n".formatted(imp));

            // generate class
            var className = "Test_%s_%d_%d_%h".formatted(
                test.filenameWithoutExtension(),
                test.line(),
                test.column(),
                test.source()
            );
            result.append("\npublic class %s {".formatted(className));
            result.append(
                "\n    @org.junit.jupiter.api.Test\n    public void test() throws Throwable {\n"
            );
            for (var line : (Iterable<String>) test.source().lines()::iterator)
                result
                    .append("        ")
                    .append(line)
                    .append("\n");
            result.append("    }\n");
            result.append("}");

            // write the file
            var filename = "%s.java".formatted(className);
            log.info("Writing the `%s` test file".formatted(filename));
            var testPath = testDirPath.resolve(filename);
            Files.writeString(testPath, result + "");
        }
    }

    public static void generateTestFiles(Logger log, Project target) throws IOException {
        var testDir = ensureTestDirExists(log, target);
        var sourceSets = sourceSets(log, target);
        var tests = enumerateMainSourceSetAndDetectTests(log, sourceSets);
        generateTestFiles(log, testDir, tests);
    }
}
