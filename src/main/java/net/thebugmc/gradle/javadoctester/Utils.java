package net.thebugmc.gradle.javadoctester;

import org.gradle.api.DefaultTask;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.util.Comparator.reverseOrder;

public final class Utils {
    private Utils() {
    }

    public static final char LINE_SEPARATOR = '\n';

    public static void clearDir(Path dirPath) throws IOException {
        if (!Files.isDirectory(dirPath)) return;
        try (var stream = Files.walk(dirPath)) {
            for (var p : (Iterable<Path>) stream.sorted(reverseOrder())::iterator)
                Files.delete(p);
        }
    }
}
